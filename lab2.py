import math as m

def fabs(a):
	return(m.fabs(a))

def sumfabs(a,b):
	return(m.fabs(a) + b)

def log2fabs(a,b):
	return(m.log2(m.fabs(a) + b))

